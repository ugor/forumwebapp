package ugorski.forumapp.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class IndexPageTest {

    @Autowired
    WebDriver webDriver;

    /**
     * Should return 'Main Page' title on index page
     */
    @Test
    public void shouldReturnIndexPageTitle() {
        //given
        webDriver.get("/");
        //when
        String title = webDriver.getTitle();
        //then
        assertThat(title).isEqualTo("Main Page");
        assertThat(title).isNotEqualTo("Profile");
    }
}