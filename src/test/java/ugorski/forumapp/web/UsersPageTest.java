package ugorski.forumapp.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UsersPageTest {

    @Autowired
    WebDriver webDriver;

    /**
     * Should return div with id 'content' on users page
     */
    @Test
    public void shouldReturnDiv() {
        webDriver.get("/users");
        String java =
                webDriver.findElement(By.id("content")).getTagName();
        assertThat(java).isEqualTo("div");
        assertThat(java).isNotEqualTo("form");
    }


}