package ugorski.forumapp.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RegisterPageTest {

    @Autowired
    WebDriver webDriver;

    /**
     * Should return h1 header 'Register' on register page
     * register random user
     * return h1 header 'Registered' on registered page
     * contains registered user email on users page
     */

    @Test
    public void shouldRegisterAndFindRegisteredUser() {

        Random random = new Random();
        String username = String.valueOf(random.nextInt(9999));
        String userEmail = username+"@test.pl";

        webDriver.get("/register");
        String header1 = webDriver.findElement(By.tagName("h1")).getText();
        assertThat(header1).isEqualTo("Register");
        WebElement email = webDriver.findElement(By.id("email"));
        WebElement name = webDriver.findElement(By.id("name"));
        WebElement password = webDriver.findElement(By.id("password"));
        email.sendKeys(userEmail);
        name.sendKeys(username);
        password.sendKeys(username);
        WebElement submit = webDriver.findElement(By.xpath("//input[@value='Submit']"));
        WebDriverWait wait = new WebDriverWait(webDriver, 1);
        wait.until(ExpectedConditions.elementToBeClickable(submit));
        submit.submit();
        String header2 = webDriver.findElement(By.tagName("h1")).getText();
        assertThat(header2).isEqualTo("Registered");
        webDriver.findElement(By.linkText("Users")).click();
        String users = webDriver.findElement(By.id("content")).getText();
        assertThat(users).contains(userEmail);
        assertThat(users).doesNotContain("10000@test.pl");
    }
}