package ugorski.forumapp.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TopicsPageTest {

    @Autowired
    WebDriver webDriver;

    /**
     * Should return 'add-thread' page after click on link on topics page
     */
    @Test
    public void shouldReturnAddThreadPageAfterClick() {
        webDriver.get("/topics");
        webDriver.findElement(By.linkText("Add thread")).click();
        assertThat(webDriver.getTitle()).isEqualTo("Add thread");
        assertThat(webDriver.getTitle()).isNotEqualTo("Add answer");
    }
}