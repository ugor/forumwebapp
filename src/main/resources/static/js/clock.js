function clock() {
    var date = new Date();

    var month = date.getMonth();
    switch(month) {
        case 0: month="JAN"; break;
        case 1: month="FEB"; break;
        case 2: month="MAR"; break;
        case 3: month="APR"; break;
        case 4: month="MAY"; break;
        case 5: month="JUN"; break;
        case 6: month="JUL"; break;
        case 7: month="AUG"; break;
        case 8: month="SEP"; break;
        case 9: month="OCT"; break;
        case 10: month="NOV"; break;
        case 11: month="DEC"; break;
    }

    var hour = date.getHours();
    if(hour<10) hour="0"+hour;
    var minute = date.getMinutes();
    if(minute<10) minute="0"+minute;
    var second = date.getSeconds();
    if(second<10) second="0"+second;

    document.getElementById("clock").innerHTML=
        date.getDate()+" "+
        month+" "+
        date.getFullYear()+" \n "+
        hour+":"+
        minute+":"+
        second;
    setTimeout(clock, 1000);
}