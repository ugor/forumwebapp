package ugorski.forumapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/video")
public class VideoRedirectController {
    @GetMapping("/forum")
    String forumRedirect(){
        return "redirect:https://www.cda.pl/video/4622663ee";
    }

    @GetMapping("/sudoku")
    String sudokuRedirect(){
        return "redirect:https://www.cda.pl/video/462235757";
    }
}
