package ugorski.forumapp.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ugorski.forumapp.entities.Answer;
import ugorski.forumapp.entities.Topic;
import ugorski.forumapp.entities.User;
import ugorski.forumapp.repositories.AnswerRepository;
import ugorski.forumapp.repositories.UserRepository;
import ugorski.forumapp.services.AnswerServis;
import ugorski.forumapp.services.TopicServis;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class TopicController {

    TopicServis topicServis;
    UserRepository userRepository;
    AnswerServis answerServis;
    AnswerRepository answerRepository;

    public TopicController(TopicServis topicServis,
                           UserRepository userRepository,
                           AnswerServis answerServis,
                           AnswerRepository answerRepository) {
        this.topicServis = topicServis;
        this.userRepository = userRepository;
        this.answerServis = answerServis;
        this.answerRepository = answerRepository;
    }

    @GetMapping("/add-topic")
    public String addTopic(Model model) {
        model.addAttribute("topic", new Topic());
        return "topic-form";
    }

    @PostMapping("/add-topic")
    @ResponseStatus(HttpStatus.CREATED)
    public String addTopic(@Valid @ModelAttribute Topic topic, BindingResult bindingResult) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        if (currentPrincipalName.equals("anonymousUser")) {
            return "not-logged";
        } else {
            User user = userRepository.findByEmail(currentPrincipalName);
            topicServis.addTopic(topic, user);
            return "topics";
        }
    }


    @GetMapping("/topics")
    public String allTopics(Model model, @RequestParam(value = "id", required = false) Long id) {
        if (id == null) {
            model.addAttribute("topics", topicServis.findAll());
            model.addAttribute("answers", answerRepository.findAll());
            return "topics";
        } else {
            AnswerDto o = new AnswerDto();
            model.addAttribute("answer", o);
            o.setTopicId(id);
            return "answer-form";
        }
    }

    @PostMapping("/add-answer")
    @ResponseStatus(HttpStatus.CREATED)
    public String addAnswer(@Valid @ModelAttribute AnswerDto answerDto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        if (currentPrincipalName.equals("anonymousUser")) {
            return "not-logged";
        } else {

            User user = userRepository.findByEmail(currentPrincipalName);

            Optional<Topic> topic = topicServis.findById(answerDto.getTopicId());
            if (!topic.isPresent()) {
                throw new RuntimeException();
            }
            Answer answer = new Answer(answerDto.getAnswer(), topic.get(), user);
            answerRepository.save(answer);
            return "topics";
        }
    }

    @GetMapping("/edit-topic")
    public String editTopics(Model model, @RequestParam(value = "id") Long id) {
        Optional topic = topicServis.findById(id);
        if (!topic.isPresent()){
            return "id-not-exist";
        } else {
            model.addAttribute("topic", topic);
        return "edit-topic-form";
        }
    }

    @PostMapping("/delete-topic")
    public String deleteTopics(@RequestParam(value = "id") Long id) {
        topicServis.deleteTopic(id);
        return "topics";
    }

    @GetMapping("/edit-answer")
    public String editAnswer(Model model, @RequestParam(value = "id") Long id) {
        model.addAttribute("answer", answerServis.findAnswer(id));
        return "edit-answer-form";
    }

    @PostMapping("/delete-answer")
    public String deleteAnswer(@RequestParam(value = "id") Long id) {
        answerServis.deleteAnswer(id);
        return "topics";
    }

    @PostMapping("/edit-answer")
    @ResponseStatus(HttpStatus.CREATED)
    public String editAnswer(@ModelAttribute Answer answer) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByEmail(currentPrincipalName);
        answerServis.addAnswer(answer, answer.getTopic(), user);
        return "topics";
    }
}
