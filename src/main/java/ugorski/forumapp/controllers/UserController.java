package ugorski.forumapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ugorski.forumapp.repositories.TopicRepository;
import ugorski.forumapp.services.UserService;

@Controller
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public String allUsers(Model model) {
        model.addAttribute("users", userService.findAll());
        return "users";
    }
}
