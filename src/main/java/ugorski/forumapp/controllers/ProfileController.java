package ugorski.forumapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ugorski.forumapp.repositories.AnswerRepository;
import ugorski.forumapp.repositories.TopicRepository;
import ugorski.forumapp.repositories.UserRepository;
import ugorski.forumapp.services.TopicServis;
import ugorski.forumapp.services.UserService;

@Controller
public class ProfileController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    AnswerRepository answerRepository;

    @GetMapping("/profile")
    public String getUserContext(Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        model.addAttribute("user", currentPrincipalName);
        model.addAttribute("topics", topicRepository.findByUser(userRepository.findByEmail(currentPrincipalName)));
        model.addAttribute("topicsCount", topicRepository.findByUser(userRepository.findByEmail(currentPrincipalName)).size());
        model.addAttribute("answers", answerRepository.findByUser(userRepository.findByEmail(currentPrincipalName)));
        model.addAttribute("answerCuntNativeQuery", answerRepository.findUsersAnswers(currentPrincipalName));
//        model.addAttribute("answersCount", answerRepository.findByUser(userRepository.findByEmail(currentPrincipalName)).size());
        return "profile";
    }
}
