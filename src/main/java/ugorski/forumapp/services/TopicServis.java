package ugorski.forumapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ugorski.forumapp.entities.Answer;
import ugorski.forumapp.entities.Topic;
import ugorski.forumapp.entities.User;
import ugorski.forumapp.repositories.TopicRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TopicServis {

    @Autowired
    private TopicRepository topicRepository;

    public void addTopic(Topic topic, User user){
        topic.setUser(user);
        topicRepository.save(topic);
    }
    public List<Topic> findUserTopic(User user){
        return topicRepository.findByUser(user);
    }

    public List<Topic> findAll(){
        return topicRepository.findAll();
    }

    public Optional<Topic> findById(Long id) {
        return topicRepository.findById(id);
    }

    public void deleteTopic(Long id){
        Optional<Topic> topic = topicRepository.findById(id);
        topicRepository.delete(topic.get());
    }
}
