package ugorski.forumapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ugorski.forumapp.entities.Answer;
import ugorski.forumapp.entities.Topic;
import ugorski.forumapp.entities.User;
import ugorski.forumapp.exceptions.AnswerNotFoundException;
import ugorski.forumapp.repositories.AnswerRepository;

import java.util.Optional;

@Service
public class AnswerServis {

    @Autowired
    private AnswerRepository answerRepository;


    public void addAnswer(Answer answer, Topic topic, User user){
        answer.setUser(user);
        answer.setTopic(topic);
        answerRepository.save(answer);
    }

    public void deleteAnswer(Long id){
        Optional<Answer> answer = answerRepository.findById(id);
        answerRepository.delete(answer.get());
    }
    public Answer findAnswer(Long id){
        Optional<Answer> answer = answerRepository.findById(id);
        if (!answer.isPresent()){
            throw new AnswerNotFoundException("Answer with id: "+id+ " not exist");
        }
        return answer.get();
    }
}
