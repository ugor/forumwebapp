package ugorski.forumapp.entities;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
public class Topic {

    @Id
    @GeneratedValue
    private Long id;
    @NotEmpty
    private String title;
    @NotEmpty
    private String description;
    @ManyToOne
    @JoinColumn(name = "USER_EMAIL")
    private User user;
    @OneToMany(mappedBy = "topic", cascade = CascadeType.ALL)
    private List<Answer> answers;

    public Topic() {
    }

    public Topic(String title, String description, User user) {
        this.title = title;
        this.description = description;
        this.user = user;
    }

    public Topic(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
