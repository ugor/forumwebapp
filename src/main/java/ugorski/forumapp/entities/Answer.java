package ugorski.forumapp.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Answer {

    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String content;
    @ManyToOne
    @JoinColumn(name = "TOPIC_ID")
    private Topic topic;
    @ManyToOne
    @JoinColumn(name = "USER_EMAIL")
    private User user;

    public Answer() {
    }

    public Answer(String content, Topic topic, User user) {
        this.content = content;
        this.topic = topic;
        this.user = user;
    }

    public Answer(String content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
