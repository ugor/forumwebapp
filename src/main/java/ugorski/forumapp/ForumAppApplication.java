package ugorski.forumapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForumAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForumAppApplication.class, args);
	}

}/*
v14 - added selenium test - login random user and check is exist on users page;
v13 - about me redirected to ugor.pl;
*/