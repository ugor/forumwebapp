package ugorski.forumapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ugorski.forumapp.entities.Answer;
import ugorski.forumapp.entities.Topic;
import ugorski.forumapp.entities.User;

import java.util.List;

public interface AnswerRepository extends JpaRepository<Answer,Long> {

    List<Answer> findByUser(User user);

    @Query(value = "select count(*) from user join answer " +
            "on user.email=answer.user_email where user.email = ?1",
            nativeQuery = true)
    Long findUsersAnswers(String userEmail);
}
