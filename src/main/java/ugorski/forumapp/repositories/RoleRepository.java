package ugorski.forumapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ugorski.forumapp.entities.Role;

public interface RoleRepository extends JpaRepository<Role,String> {
}
