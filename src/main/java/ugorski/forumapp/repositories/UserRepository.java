package ugorski.forumapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ugorski.forumapp.entities.User;

public interface UserRepository extends JpaRepository<User,String> {
    User findByEmail(String username);

    User findByName(String username);
}
