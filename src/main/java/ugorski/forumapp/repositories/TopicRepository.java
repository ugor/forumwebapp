package ugorski.forumapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ugorski.forumapp.entities.Topic;
import ugorski.forumapp.entities.User;

import java.util.List;
import java.util.Optional;

public interface TopicRepository extends JpaRepository<Topic,Long> {
    List<Topic> findByUser(User user);
}
